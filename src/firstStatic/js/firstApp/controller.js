(function() {
	angular.module('someModule').controller('someController', someController);

	function someController() {
		console.log('launched!');
	}
})();
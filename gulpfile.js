var gulp = require('gulp');
var gulpSequence = require('gulp-sequence')
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var less = require('gulp-less');
var fs = require('fs');
var path = require('path');
var del = require('del');
var autoprefixer = require('gulp-autoprefixer');
var templateCache = require('gulp-angular-templatecache');
var merge = require('merge-stream');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var include = require("gulp-include");
var server = require('browser-sync').create()

var defaultStaticResourceMeta =
'<?xml version="1.0" encoding="UTF-8"?>\n\
<StaticResource xmlns="http://soap.sforce.com/2006/04/metadata">\n\
    <cacheControl>Public</cacheControl>\n\
    <contentType>application/javascript</contentType>\n\
</StaticResource>';

var defaultPackageXML =
'<?xml version="1.0" encoding="UTF-8"?>\n\
<Package xmlns="http://soap.sforce.com/2006/04/metadata">\n\
	<types>\n\
		<members>*</members>\n\
		<name>StaticResource</name>\n\
	</types>\n\
	<version>34.0</version>\n\
</Package>';

var constants = {
	srcFolder: 'src',
	prebuildFolder: 'prebuild',
	buildFolder: 'package',
	bowerFolder: 'bower_components'
};

gulp.task('deploy', function() {
   gulp.src('./package/**', { base: "." })
    .pipe(zip('pkg.zip'))
    .pipe(forceDeploy(
    	{
    		loginUrl: '',
    		username: '',
    		password: ''
    	}
    ))
});

gulp.task('copy', function() {
    if (fs.existsSync(constants.prebuildFolder)) {
        del.sync(constants.prebuildFolder + '/*');
    } else {
        fs.mkdirSync(constants.prebuildFolder);
    } 
    var srcStream = gulp.
    	src(constants.srcFolder + '/**/*', { base: constants.srcFolder}).
        pipe(gulp.dest(constants.prebuildFolder));

   	var bowerStream = gulp.
   		src(constants.bowerFolder + '/**/*', { base: '.'}).
   		pipe(gulp.dest(constants.prebuildFolder));

   	return merge(srcStream, bowerStream);
});

gulp.task('build', ['prebuild'], function() {
	if (fs.existsSync(constants.buildFolder)) {
        del.sync(constants.buildFolder + '/staticresources/*.resource');
    } else {
        fs.mkdirSync(constants.buildFolder);
        fs.mkdirSync(constants.buildFolder + '/staticresources');
    }

	var prebuildFolders = getPrebuildFolders(constants.prebuildFolder);

	if (fs.existsSync(constants.prebuildFolder + '/' + constants.bowerFolder)) {
		prebuildFolders.push(constants.bowerFolder);
	}
	var templateStreams = merge();

	createFileIfNotExists(constants.buildFolder + '/package.xml', defaultPackageXML);
	for (folder in prebuildFolders) {
		var fullFolderName = constants.prebuildFolder + '/' + prebuildFolders[folder];

		var newStream = gulp.src(fullFolderName + '/**').
			pipe(zip(prebuildFolders[folder] + '.resource')).
		pipe(gulp.dest(constants.buildFolder + '/staticresources'));

		templateStreams.add(newStream);
		createFileIfNotExists(constants.buildFolder + '/staticresources/' + prebuildFolders[folder] + '.resource-meta.xml', defaultStaticResourceMeta);
	}
	return templateStreams;
});

gulp.task('angularTemplates', function() {
	var prebuildFolders = getPrebuildFolders(constants.prebuildFolder);
	
	var templateStreams = merge();

	for (folder in prebuildFolders) {
		var fullFolderName = constants.prebuildFolder + '/' + prebuildFolders[folder];

		var newStream = gulp.src(fullFolderName + '/**/templates/*.html').	   
	    pipe(templateCache('templates.js', {
	        base: __dirname + '/' + constants.prebuildFolder,
	        root: '',
	        module: prebuildFolders[folder] + 'Templates',
	        standalone: true
	    })).	  
	    pipe(gulp.dest(fullFolderName + '/js'));

	    templateStreams.add(newStream);
	}
	
    return templateStreams;
});

gulp.task('babel', function() {
	var prebuildFolders = getPrebuildFolders(constants.prebuildFolder);
	var templateStreams = merge();

	for (folder in prebuildFolders) {
		var fullFolderName = constants.prebuildFolder + '/' + prebuildFolders[folder];
		var newStream = gulp.src(fullFolderName + '/js/**/*.js').
			pipe(sourcemaps.init()).
				pipe(include()).
					on('error', console.log).
				pipe(babel({
					presets: ['es2015']
				})).
			pipe(sourcemaps.write('.')).
			pipe(gulp.dest(fullFolderName + '/js'));
		templateStreams.add(newStream);
	}
	return templateStreams;
});

gulp.task('css', function(callback) {
	var prebuildFolders = getPrebuildFolders(constants.prebuildFolder);	
	var cssStreams = merge();

	for (folder in prebuildFolders) {
		var prefixes = autoprefixer({
	        browsers: ['last 2 versions'],
	        cascade: false
	    });

		var fullFolderName = constants.prebuildFolder + '/' + prebuildFolders[folder];

		var newStream = gulp.src(fullFolderName + '/less/*.less').
			pipe(sourcemaps.init()).
				pipe(less()).
				pipe(prefixes).
				pipe(concat('all.css')).
			pipe(sourcemaps.write()).
			pipe(gulp.dest(fullFolderName + '/css'));

		cssStreams.add(newStream);
	}

	return cssStreams;
});

gulp.task('js', ['babel', 'angularTemplates']);

gulp.task('prebuild', function(callback) {gulpSequence('copy', ['css', 'js'])(callback);});
gulp.task('default', function(callback) {gulpSequence('build', 'deploy')(callback);});

gulp.task('server', function(callback) {
	server.init({
		server: {
			baseDir: constants.prebuildFolder,
			https: true,
			cors: true
		}
	});
});

gulp.task('watch', ['prebuild'], function() {
	gulp.start('server');
    gulp.watch('src/**', ['prebuild']);
});

function getPrebuildFolders() {
  return fs.readdirSync(constants.prebuildFolder).filter(function(file) {
    return fs.statSync(path.join(constants.prebuildFolder, file)).isDirectory() && file != constants.bowerFolder;
  });
}

function createFileIfNotExists(fileName, content) {
	if (!fs.existsSync(fileName)) {
		fs.writeFileSync(fileName, content, {flags: 'w'});
	}
}